package org.champ.acar2carexpenses;


import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, ParseException {
        List<FuelRecord> records = ACarReader.readFuelRecords(args[0]);
        System.out.println("loaded records: " + records.size());
        CarExpensesWriter.writeFuelRecords(args[1], "Forester", records);
    }
}
