package org.champ.acar2carexpenses;

import java.io.*;
import java.text.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * @author Igor Tyazhev (tyazhev@gmail.com) Date: 08.12.14 23:41
 */

/*
Поля делятся на два вида:
Вводные, которые вносит пользователь. Большая часть из них - необходима для расчета
date - дата
note - заметка (необяз)
mileage - пробег
volume - объем
volumecost - цена литра
cost - цена чека
type - тип топлива
full - устарелое поле (означало заправку до полного) - сейчас означает как до полного, так и к.т.
mark - тип записи, коих много:

1 - путевая точка (пробег +дата)
2 - дозаправка без пробега
3 - резерв
4 - дозаправка с пробегом
5 - резерв
6 - контр. точка
7 - резерв
8 - контр.точка + дозаправка
9 - резерв
10 - полный бак
11 - резерв
12 - полный бак + к.т.

Поля, которые рассчитываются при каждом вызове пересчета (по добавлению записи, переключении ТС или кнопка вверху гл. страницы) и попадают в экспорт в качестве справочной информации.
costmileage - цена пути
mileagevolume - обратно-пропорциональная величина расходу - сколько км на 1 л топлива
volumemileage - расход
mileageadd - разница пробега от предыдущей записи
tankvolume - остаток в баке (сейчас не заполняется)
latitude - широта (сейчас не заполняется)
longtitude - долгота (сейчас не заполняется)
*/

public class CarExpensesWriter {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
    private static final String LE = "\r\n";

    public static void writeFuelRecords(String fileName, String idVehicle, List<FuelRecord> records) throws IOException {
        NumberFormat intFormat = new DecimalFormat("##0");
        NumberFormat decimalFormat = NumberFormat.getCurrencyInstance(new Locale("RU"));
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        ((DecimalFormat) decimalFormat).setDecimalFormatSymbols(dfs);
        ((DecimalFormat) decimalFormat).setGroupingUsed(false);
        Collections.sort(records, new Comparator<FuelRecord>() {
            @Override
            public int compare(FuelRecord o1, FuelRecord o2) {
                return o1.getOdometer() - o2.getOdometer();
            }
        });
        StringBuilder sb = new StringBuilder();
        sb.append("### CE").append(LE);
        sb.append("### records info").append(LE);
        final String tableColumns = "id_vehicle      \tdate       \tnote                                               \tmileage     \tvolume   \tvolumecost   \tcost               \ttype            \tfull   \tmark   \tcostmileage     \tmileagevolume   \tvolumemileage   \tmileageadd      \ttankvolume \tlatitude        \tlongtitude     ";
        String[] columns = tableColumns.split("\t");
        sb.append(tableColumns).append(LE);
        for (FuelRecord record : records) {
            sb.append(/*id_vehicle*/addPadding(idVehicle, columns[0].length())).append('\t');
            sb.append(/*date*/addPadding(DATE_FORMAT.format(record.getDate()), columns[1].length())).append('\t');
            sb.append(/*note*/addPadding(" ", columns[2].length())).append('\t');
            sb.append(/*mileage*/addPadding(intFormat.format(record.getOdometer()), columns[3].length())).append('\t');
            sb.append(/*volume*/addPadding(decimalFormat.format(record.getVolume()), columns[4].length())).append('\t');
            sb.append(/*volumecost*/addPadding(decimalFormat.format(record.getPrice()), columns[5].length())).append('\t');
            sb.append(/*cost*/addPadding(decimalFormat.format((record.getCost())), columns[6].length())).append('\t');
            sb.append(/*type*/addPadding("АИ-95", columns[7].length())).append('\t');
            sb.append(/*full*/addPadding("0", columns[8].length())).append('\t');
            sb.append(/*mark*/addPadding(record.isFull() ? "10" : "4", columns[9].length())).append('\t');

            sb.append(addPadding("0", columns[10].length())).append('\t');
            sb.append(addPadding("0", columns[11].length())).append('\t');
            sb.append(addPadding("0", columns[12].length())).append('\t');
            sb.append(addPadding("0", columns[13].length())).append('\t');
            sb.append(addPadding("0", columns[14].length())).append('\t');
            sb.append(addPadding("0", columns[15].length())).append('\t');
            sb.append(addPadding("0", columns[16].length())).append('\t');

            sb.append(LE);
        }
        //System.out.println("champ: sb=" + sb);
        Writer fw = null;
        try {
            fw = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName), "UTF-8"));;
            fw.write(sb.toString());
        } finally {
            if (fw != null) {
                fw.close();
            }
        }
    }

    private static String addPadding(String text, int length) {
        StringBuilder result = new StringBuilder(text);
        while(result.length() < length) {
            result.append(" ");
        }
        return result.toString();
    }
}
