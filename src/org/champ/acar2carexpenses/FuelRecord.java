package org.champ.acar2carexpenses;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Igor Tyazhev (tyazhev@gmail.com) Date: 08.12.14 22:30
 */
public class FuelRecord {
    private final int id;
    private final Date date;
    private final int odometer;
    private final int odometerDiff;
    private final BigDecimal volume;
    private final BigDecimal price;
    private final BigDecimal cost;
    private final boolean isFull;

    public FuelRecord(int id, Date date, int odometer, int odometerDiff, BigDecimal volume, BigDecimal price, BigDecimal cost, boolean isFull) {
        this.id = id;
        this.date = date;
        this.odometer = odometer;
        this.odometerDiff = odometerDiff;
        this.volume = volume;
        this.price = price;
        this.cost = cost;
        this.isFull = isFull;
    }


    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public int getOdometer() {
        return odometer;
    }

    public int getOdometerDiff() {
        return odometerDiff;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public boolean isFull() {
        return isFull;
    }

    @Override
    public String toString() {
        return "FuelRecord{" +
                "id=" + id +
                ", date=" + date +
                ", odometer=" + odometer +
                ", odometerDiff=" + odometerDiff +
                ", volume=" + volume +
                ", price=" + price +
                ", cost=" + cost +
                ", isFull=" + isFull +
                '}';
    }
}
