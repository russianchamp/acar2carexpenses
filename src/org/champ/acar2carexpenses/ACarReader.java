package org.champ.acar2carexpenses;

import com.sun.org.apache.xerces.internal.dom.DeferredElementImpl;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Igor Tyazhev (tyazhev@gmail.com) Date: 08.12.14 22:37
 */
public class ACarReader {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy - hh:mm");

    public static List<FuelRecord> readFuelRecords(String fileName) throws ParserConfigurationException, IOException, SAXException, ParseException {
        List<FuelRecord> result = new ArrayList<FuelRecord>();
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse("file:" + fileName);
        NodeList nodes = document.getElementsByTagName("fillup-record");
        for(int i = 0; i < nodes.getLength(); i++) {
            DeferredElementImpl node = (DeferredElementImpl) nodes.item(i);
            int id = Integer.parseInt(node.getAttributes().getNamedItem("id").getNodeValue());
            Date date = DATE_FORMAT.parse(node.getElementsByTagName("date").item(0).getTextContent());
            int odometerDiff = new BigDecimal(node.getElementsByTagName("driven-distance").item(0).getTextContent()).intValue();
            int odometer = new BigDecimal(node.getElementsByTagName("odometer-reading").item(0).getTextContent()).intValue();
            BigDecimal price = new BigDecimal(node.getElementsByTagName("price-per-volume-unit").item(0).getTextContent());
            BigDecimal volume = new BigDecimal(node.getElementsByTagName("volume").item(0).getTextContent());
            BigDecimal cost = new BigDecimal(node.getElementsByTagName("total-cost").item(0).getTextContent());
            boolean isFull = !Boolean.parseBoolean(node.getElementsByTagName("partial").item(0).getTextContent());
            result.add(new FuelRecord(id, date, odometer, odometerDiff, volume, price, cost, isFull));
        }
        return result;
    }
}
